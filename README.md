# Api documentation
* First you need to install dependencies (requirements/development.txt)
* You need to create a file .env inside the base project, and  put the SECRET_KEY
* Create a database_name and name it pokemondb  

## 1. Command to fetch and stores pokemon data and evolutions
- Command: python manage.py get_pokemon_data 67
    
## 2. Get pokemon detail
- Api: http://localhost:8000/pokemon/<str:name>/
- Method: get
- Example : http://localhost:8000/pokemon/ivysaur/
```json
{
    "id": 2,
    "name": "ivysaur",
    "base_stats": [
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/1/",
                "name": "hp"
            },
            "effort": 0,
            "base_stat": 60
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/2/",
                "name": "attack"
            },
            "effort": 0,
            "base_stat": 62
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/3/",
                "name": "defense"
            },
            "effort": 0,
            "base_stat": 63
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/4/",
                "name": "special-attack"
            },
            "effort": 1,
            "base_stat": 80
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/5/",
                "name": "special-defense"
            },
            "effort": 1,
            "base_stat": 80
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/6/",
                "name": "speed"
            },
            "effort": 0,
            "base_stat": 60
        }
    ],
    "height": 10,
    "weight": 130,
    "evolves": [
        {
            "id": 1,
            "evolution_type": "Preevolution",
            "name": "bulbasaur"
        },
        {
            "id": 3,
            "evolution_type": "Evolution",
            "name": "venusaur"
        }
    ]
}
```

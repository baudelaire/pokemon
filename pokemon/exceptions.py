class PokemonExistsException(Exception):
    pass


class EvolutionChainNotExistsException(Exception):
    pass

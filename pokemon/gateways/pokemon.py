import requests
from django.conf import settings
from rest_framework.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from pokemon.exceptions import EvolutionChainNotExistsException


class PokemonGateway:
    def __init__(self):
        self.pokemon_base = settings.POKEAPI_BASE_URL
        self.evolution_chain = settings.POKEAPI_EVOLUTION_CHAIN_URL
        self.headers = {'Content-Type': 'application/json'}

    def get_evolution_chain_data(self, id):
        try:
            response = requests.get(
                self.evolution_chain + f'{id}/', headers=self.headers,
                timeout=2
            )
            if response.status_code == 200:
                return response.json()
            else:
                raise EvolutionChainNotExistsException(
                    _("This evolution_chain_id not exists")
                )

        except (
                ValidationError, requests.exceptions.Timeout,
                requests.exceptions.ConnectionError,
                EvolutionChainNotExistsException) as exc:
            raise exc

    def get_pokemon_data(self, name=None):
        try:
            response = requests.get(
                self.pokemon_base + f'{name}/', headers=self.headers,
                timeout=2
            )
            return response.json()
        except (
                ValidationError, requests.exceptions.Timeout,
                requests.exceptions.ConnectionError
        ) as exc:
            raise exc

from django.core.management.base import BaseCommand, CommandError
from pokemon.gateways import PokemonGateway
from pokemon.services import PokemonService


class Command(BaseCommand):
    help = 'Save pokemon data to db'

    def add_arguments(self, parser):
        parser.add_argument('chain_id', type=int)

    def handle(self, *args, **kwargs):
        try:
            response = PokemonGateway().get_evolution_chain_data(
                id=kwargs.get('chain_id')
            )
            PokemonService().save_pokemon_data(response['chain'], None)
        except Exception as e:
            raise CommandError(str(e))

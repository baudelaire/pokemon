from django.db import models
from django.contrib.postgres.fields import JSONField
from django.db.models import Q


class Pokemon(models.Model):
    pokemon_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, unique=True)
    base_stats = JSONField()
    height = models.SmallIntegerField()
    weight = models.SmallIntegerField()
    id = models.PositiveIntegerField(unique=True)
    evolves_from = models.ForeignKey(
        'self', on_delete=models.SET_NULL, related_name='evolutions', null=True
    )

    class Meta:
        db_table = 'pokemon'

    def __str__(self):
        return self.name

    def get_ancestors(self, list_ids):
        if self.evolves_from:
            list_ids.append(self.evolves_from.pk)
            return self.evolves_from.get_ancestors(list_ids)
        return Pokemon.objects.filter(pk__in=list_ids)

    def get_evolutions(self):
        evolution_list = [self]
        if not hasattr(self, 'evolutions'):
            return evolution_list

        evolutions = self.evolutions.all()

        for evolution in evolutions:
            evolution_list.extend(evolution.get_evolutions())

        return evolution_list

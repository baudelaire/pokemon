from rest_framework import serializers

from pokemon.models import Pokemon


class EvolvesSerializer(serializers.ModelSerializer):
    evolution_type = serializers.SerializerMethodField()

    class Meta:
        model = Pokemon
        fields = ['id', 'evolution_type', 'name']

    def get_evolution_type(self, obj):
        if obj.pk > self.context['pokemon'].pk:
            return 'Evolution'
        return 'Preevolution'


class PokemonSerializer(serializers.ModelSerializer):
    evolves = serializers.SerializerMethodField()

    class Meta:
        model = Pokemon
        fields = [
            'id', 'name', 'base_stats', 'height', 'weight', 'evolves',
        ]

    def get_evolves(self, obj):
        evolutions_ids = [pokemon.pk for pokemon in obj.get_evolutions()]
        evolutions_ids.remove(obj.pk)
        evolutions = Pokemon.objects.filter(pk__in=evolutions_ids)

        evolves1 = obj.get_ancestors([]) | evolutions
        return EvolvesSerializer(
            evolves1.all(), many=True, context={'pokemon': obj}
        ).data

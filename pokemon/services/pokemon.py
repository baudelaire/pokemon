from pokemon.exceptions import PokemonExistsException
from pokemon.gateways import PokemonGateway
from pokemon.models import Pokemon
from django.utils.translation import ugettext_lazy as _


class PokemonService:
    def save_pokemon_data(self, response=None, pre_evolution=None, ):
        """
        Save all pokemon data according evolution chain response, recursively
        """
        name = response['species']['name']

        if Pokemon.objects.filter(name=name).exists():
            raise PokemonExistsException(_('This Pokemon already exists'))

        pokemon_response = PokemonGateway().get_pokemon_data(name)
        pre_evolution = Pokemon.objects.create(
            name=name,
            id=pokemon_response['id'],
            base_stats=pokemon_response['stats'],
            height=pokemon_response['height'],
            weight=pokemon_response['weight'],
            evolves_from=pre_evolution
        )
        for evolve_response in response['evolves_to']:
            self.save_pokemon_data(evolve_response, pre_evolution)

        return pre_evolution

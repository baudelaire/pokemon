import os
from configurations import values

from .common import Common


class Local(Common):

    DEBUG = values.BooleanValue(True)

    ALLOWED_HOSTS = ['*']

    SECRET_KEY = os.getenv("SECRET_KEY")

    DATABASES = values.DatabaseURLValue(
        'postgres://postgres@localhost/pokemondb'
    )

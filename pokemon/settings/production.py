import os
from configurations import values

from .common import Common


class Production(Common):

    DEBUG = False

    SECRET_KEY = os.getenv("SECRET_KEY")

    ALLOWED_HOSTS = ['api.pokemon.com']

    DATABASES = values.DatabaseURLValue(
        'postgis://postgres@localhost/pokemondb',
        environ_name='PRODUCTION_POKEMON_DATABASE_URL',
    )

    
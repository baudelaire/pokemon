from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.response import Response

from pokemon.models import Pokemon
from pokemon.serializers import PokemonSerializer


class PokemonDetailView(generics.RetrieveAPIView):
    lookup_field = 'name'
    serializer_class = PokemonSerializer
    queryset = Pokemon.objects.none()

    def retrieve(self, request, *args, **kwargs):
        instance = get_object_or_404(
            Pokemon.objects.all(), name=kwargs['name']
        )
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
